package sample.declarative;

import org.junit.jupiter.api.Test;
import sample.declarative.codec.CodecChecker;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.declarative.TelegramDeclaration.FULL_TELEGRAM;

class TelegramCodecValidation {
    @Test
    void should_validate_telegram_checker() {
        final CodecChecker checker = new CodecChecker();
        assertTrue(FULL_TELEGRAM.check(checker));
        assertTrue(checker.isValid());
    }
}
