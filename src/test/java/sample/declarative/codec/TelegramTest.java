package sample.declarative.codec;

import io.vavr.test.Arbitrary;
import io.vavr.test.Gen;
import io.vavr.test.Property;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static sample.declarative.codec.Telegram.empty;
import static sample.declarative.codec.Telegram.telegram;
import static sample.declarative.codec.TelegramChunk.bit;
import static sample.declarative.codec.TelegramChunk.chunk;

class TelegramTest {
    @Test
    void should_get_boolean_at_chunk_position() {
        assertTrue(telegram("1").getBool(bit(1, 0)));
    }

    @Test
    void should_set_boolean_at_chunk_position() {
        final Arbitrary<Integer> line = Arbitrary.ofAll(Gen.choose(1, 60));
        final Arbitrary<Integer> offset = Arbitrary.ofAll(Gen.choose(0, 7));

        Property.def("get/set boolean")
                .forAll(line, offset)
                .suchThat((l, o) -> empty().setBool(bit(l, o), true).getBool(bit(l, o)))
                .check()
                .assertIsSatisfied();
    }

    @Test
    void should_get_integer_at_chunk_position() {
        assertEquals(69, telegram("11000101").getInt(chunk(1, 1, 7)));
    }

    @Test
    void should_set_integer_at_chunk_position() {
        final Arbitrary<Integer> line = Arbitrary.ofAll(Gen.choose(1, 60));
        final Arbitrary<Integer> offset = Arbitrary.ofAll(Gen.choose(0, 7));
        final Arbitrary<Integer> size = Arbitrary.ofAll(Gen.choose(0, 7));
        final Arbitrary<Integer> value = Arbitrary.ofAll(Gen.choose(0, 255));

        Property.def("get/set boolean")
                .forAll(line, offset, size, value)
                .suchThat((l, o, s, v) -> {
                    final int expected = v % (1 << s);
                    return empty().setInt(chunk(l, o, s), expected).getInt(chunk(l, o, s)) == expected;
                })
                .check()
                .assertIsSatisfied();
    }
}
