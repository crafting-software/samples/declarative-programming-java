package sample.declarative;

import sample.declarative.document.TextDocument;

import static sample.declarative.TelegramDeclaration.FULL_TELEGRAM;

public class GenerateTelegramDocumentation {

    public static void main(final String[] args) {
        final TextDocument document = new TextDocument();
        FULL_TELEGRAM.document(document);
        System.out.println(document.render());
    }
}
