package sample.declarative.commandencoding;

public class CommandChunk {
    private final String value;
    private final Position position;

    public CommandChunk(String value, Position position) {
        this.value = value;
        this.position = position;
    }

    @Override
    public String toString() {
        return "CommandChunk{" +
                "value='" + value + '\'' +
                ", position=" + position +
                '}';
    }
}
