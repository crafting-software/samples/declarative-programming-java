package sample.declarative.commandencoding;

public class Position {
    private final int start;
    private final int stop;

    public static Position at(final int start, final int stop) {
        return new Position(start, stop);
    }

    private Position(final int start, final int stop) {
        this.start = start;
        this.stop = stop;
    }

    @Override
    public String toString() {
        return "Position{" +
                "start=" + start +
                ", stop=" + stop +
                '}';
    }
}
