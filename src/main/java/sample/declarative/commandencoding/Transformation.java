package sample.declarative.commandencoding;


import java.util.Map;

public interface Transformation {
    void process(final Map<String, String> input, final Map<String, CommandChunk> output);


    String document();
}
