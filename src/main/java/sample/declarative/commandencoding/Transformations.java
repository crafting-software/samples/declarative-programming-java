package sample.declarative.commandencoding;

import java.util.Arrays;
import java.util.Map;

import static java.util.stream.Collectors.joining;

public class Transformations {
    public static Transformation transformation(final Transformation... transformations) {
        return new Transformation() {
            public void process(Map<String, String> input, Map<String, CommandChunk> output) {
                for (final Transformation transformation : transformations) {
                    transformation.process(input, output);
                }
            }

            public String document() {
                return Arrays.stream(transformations).map(Transformation::document).collect(joining("\n"));
            }
        };
    }

    public static Transformation move(final String property, final Position position) {
        return new Transformation() {
            public void process(Map<String, String> input, Map<String, CommandChunk> output) {
                output.put(property, new CommandChunk(input.get(property), position));
            }

            public String document() {
                return "move property " + property + " into position " + position;
            }
        };
    }

    public static Transformation set(final String property, final String value, final Position position) {
        return new Transformation() {
            public void process(Map<String, String> input, Map<String, CommandChunk> output) {
                output.put(property, new CommandChunk(value, position));
            }

            public String document() {
                return "set property " + property + " to " + value + " into position " + position;
            }
        };
    }
}
