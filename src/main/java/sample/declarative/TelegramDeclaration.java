package sample.declarative;

import sample.declarative.codec.TelegramCodec;

import static sample.declarative.codec.Case.on;
import static sample.declarative.codec.Codecs.*;
import static sample.declarative.codec.TelegramChunk.bit;
import static sample.declarative.codec.TelegramChunk.chunk;

public class TelegramDeclaration {

    private static final TelegramCodec BOOLEAN_TELEGRAM = named(
            "Boolean Section",
            "This is the boolean section of the telegram",
            bool(bit(2, 1), "my.conditional.bool"));

    private static final TelegramCodec INTEGER_TELEGRAM = integer(chunk(2, 1, 7), "my.conditional.int");

    public static final TelegramCodec FULL_TELEGRAM = named(
            "Full Telegram",
            "This is the common part of the telegram",
            compose(
                    bool(bit(1, 0), "my.boolean"),
                    integer(chunk(1, 1, 7), "my.integer"),
                    condition(chunk(2, 0, 1), "my.kind",
                            on(0, BOOLEAN_TELEGRAM),
                            on(1, INTEGER_TELEGRAM))
            ));
}
