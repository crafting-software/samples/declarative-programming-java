package sample.declarative;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import sample.declarative.commandencoding.CommandChunk;
import sample.declarative.commandencoding.Transformation;

import java.util.Map;

import static sample.declarative.commandencoding.Position.at;
import static sample.declarative.commandencoding.Transformations.*;

public class CommandDemo {

    private static Transformation setRow = transformation(move("element-id", at(44, 63)), set("info-type", "70", at(23, 40)));

    private static Transformation pointSwitch = transformation(move("element-id", at(20, 32)), set("point-type", "14", at(23, 40)));

    private static Map<String, Transformation> TRANSFORMATIONS = ImmutableMap.of(
            "SetRow", setRow,
            "PointSwitch", pointSwitch
    );


    public static void main(final String[] args) {
        playwithcommand();
    }

    private static void playwithcommand() {
        final Map<String, String> input = Map.of("element-id", "FTG-HTY-RED");
        final Map<String, CommandChunk> output = Maps.newHashMap();
        pointSwitch.process(input, output);
        System.out.println(output);

        System.out.println(pointSwitch.document());
    }
}
