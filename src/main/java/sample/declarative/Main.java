package sample.declarative;

import sample.declarative.codec.Signal;
import sample.declarative.codec.Telegram;

import static sample.declarative.TelegramDeclaration.FULL_TELEGRAM;

public class Main {

    public static void main(final String[] args) {
        decoding();
        encoding();
    }

    private static void decoding() {
        System.out.println("\nDecoding: ");
        final Telegram telegram = Telegram.telegram("10000101 | 10000001");
        final Signal signal = Signal.empty();
        FULL_TELEGRAM.decode(telegram, signal);

        System.out.println("Telegram: " + telegram.content());
        System.out.println("Signal:");
        System.out.println(signal.content());
    }

    private static void encoding() {
        System.out.println("\nEncoding: ");
        final Signal signal = Signal.empty();
        signal.setBool("my.boolean", true);
        signal.setInt("my.integer", 6);
        signal.setInt("my.kind", 1);
        signal.setInt("my.conditional.int", 42);
        final Telegram telegram = Telegram.empty();
        FULL_TELEGRAM.encode(telegram, signal);

        System.out.println("Signal:");
        System.out.println(signal.content());
        System.out.println("Telegram: " + telegram.content());
    }
}
