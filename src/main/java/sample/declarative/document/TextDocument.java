package sample.declarative.document;

import com.google.common.collect.Maps;
import sample.declarative.codec.TelegramChunk;
import sample.declarative.codec.Document;

import java.util.Map;
import java.util.Stack;

public final class TextDocument implements Document {
    private final Map<String, TextSection> sections = Maps.newLinkedHashMap();
    private final Stack<String> index = new Stack<>();
    private final TextSection main = new TextSection("Main", "Default section");

    public void beginSection(final String name, final String description) {
        section().reference(name);
        if (!sections.containsKey(name)) {
            sections.put(name, new TextSection(name, description));
        }
        index.add(name);
    }

    public void endSection(final String name, final String description) {
        index.pop();
    }

    public void integerRule(final TelegramChunk chunk, final String property) {
        section().integerRule(chunk, property);
    }

    public void booleanRule(final TelegramChunk chunk, final String property) {
        section().booleanRule(chunk, property);
    }

    public void startConditional(final TelegramChunk chunk, final String property) {
        section().startConditional(chunk, property);
    }

    public void endConditional(final TelegramChunk chunk, final String property) {
        section().endConditional();
    }

    public void startWhen(final int value) {
        section().startWhen(value);
    }

    public void endWhen(final int value) {
        section().endWhen();
    }

    private TextSection section() {
        if (index.size() > 0) {
            return sections.get(index.peek());
        }
        return main;
    }

    public String render() {
        final StringBuilder builder = new StringBuilder();
        if (main.hasContent()) {
            builder.append(main.render()).append("\n\n");
        }
        for (final TextSection value : sections.values()) {
            if (value.hasContent()) {
                builder.append(value.render()).append("\n\n");
            }
        }
        return builder.toString();
    }
}
