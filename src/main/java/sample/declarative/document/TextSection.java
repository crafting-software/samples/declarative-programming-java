package sample.declarative.document;

import com.google.common.base.Strings;
import sample.declarative.codec.TelegramChunk;

final class TextSection {
    private final StringBuilder builder = new StringBuilder();
    private int indentation = 0;
    private boolean empty = true;

    TextSection(final String name, final String description) {
        builder.append("== ").append(name).append("\n\n").append(description).append("\n");
        indent();
        newline();
    }

    void integerRule(final TelegramChunk chunk, final String property) {
        builder.append("- integer at ").append(chunk.document()).append(" is property ").append(property);
        newline();
        empty = false;
    }

    void booleanRule(final TelegramChunk chunk, final String property) {
        builder.append("- boolean at ").append(chunk.document()).append(" is property ").append(property);
        newline();
        empty = false;
    }

    void startConditional(final TelegramChunk chunk, final String property) {
        builder.append("- condition at ").append(chunk.document()).append(" is property ").append(property);
        indent();
        newline();
        empty = false;
    }

    void endConditional() {
        deindent();
        newline();
        empty = false;
    }

    void startWhen(final int value) {
        builder.append("# when ").append(value).append(":");
        indent();
        newline();
        empty = false;
    }

    void endWhen() {
        deindent();
        newline();
        empty = false;
    }

    void reference(final String name) {
        builder.append("<ref ").append(name).append(">");
        newline();
    }

    boolean hasContent() {
        return !empty;
    }

    private void newline() {
        builder.append("\n");
        builder.append(Strings.repeat(" ", indentation));
    }

    private void indent() {
        indentation += 4;
    }

    private void deindent() {
        indentation -= 4;
    }

    String render() {
        return builder.toString();
    }
}
