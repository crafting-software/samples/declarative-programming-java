package sample.declarative.codec;

import java.util.BitSet;

public final class TelegramChunk {
    private final static int[] MASK = {0, 0b1, 0b11, 0b111, 0b1111, 0b11111, 0b111111, 0b1111111, 0b11111111};
    private final int line;
    private final int offset;
    private final int size;

    public static TelegramChunk bit(final int line, final int offset) {
        return new TelegramChunk(line, offset, 1);
    }

    public static TelegramChunk chunk(final int line, final int offset, final int size) {
        return new TelegramChunk(line, offset, size);
    }

    private TelegramChunk(final int line, final int offset, final int size) {
        this.line = line;
        this.offset = offset;
        this.size = size;
    }

    public int line() {
        return line;
    }

    public int offset() {
        return offset;
    }

    public int size() {
        return size;
    }

    boolean getBoolOf(final BitSet content) {
        return content.get(index());
    }

    void setBool(final BitSet content, final boolean value) {
        content.set(index(), value);
    }

    int getIntOf(final BitSet content) {
        int result = 0;
        for (int i = 0; i < size; i++) {
            result <<= 1;
            result |= content.get(index() + i) ? 1 : 0;
        }
        return result;
    }

    void setInt(final BitSet content, final int value) {
        int v = value & MASK[size];
        int offset = index() + size - 1;
        while (v != 0) {
            if (v % 2 != 0) {
                content.set(offset);
            }
            --offset;
            v = v >>> 1;
        }
    }

    public String document() {
        return "line: " + line + "; offset: " + offset + "; size: " + size;
    }

    int index() {
        return (line - 1) * 8 + offset;
    }
}
