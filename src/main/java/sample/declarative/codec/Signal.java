package sample.declarative.codec;

import com.google.common.collect.Maps;

import java.util.Map;

public final class Signal {
    private final Map<String, Object> values;

    public static Signal empty() {
        return new Signal(Maps.newHashMap());
    }

    private Signal(final Map<String, Object> values) {
        this.values = values;
    }

    public boolean getBool(final String property) {
        return (boolean) values.getOrDefault(property, false);
    }

    public void setBool(final String property, final boolean value) {
        values.put(property, value);
    }

    public int getInt(final String property) {
        return (int) values.getOrDefault(property, 0);
    }

    public void setInt(final String property, final int value) {
        values.put(property, value);
    }

    public String content() {
        final StringBuilder builder = new StringBuilder();
        for (final Map.Entry<String, Object> entry : values.entrySet()) {
            builder.append(entry.getKey()).append(": ").append(entry.getValue()).append("\n");
        }
        return builder.toString();
    }
}
