package sample.declarative.codec;

import java.util.BitSet;
import java.util.List;
import java.util.Set;

import static com.google.common.collect.Sets.newHashSet;

public final class CodecChecker {
    private final BitSet frame;

    public CodecChecker() {
        this(new BitSet());
    }

    public CodecChecker(final BitSet frame) {
        this.frame = frame;
    }

    public boolean checkBool(final String property, final TelegramChunk chunk) {
        return checkInt(property, chunk);
    }

    public boolean checkInt(final String property, final TelegramChunk chunk) {
        final int start = chunk.index();
        for (int i = 0; i < chunk.size(); i++) {
            if (frame.get(start + i)) {
                throw new RuntimeException("Chunk definition of " + property + " is overlapping");
            }
            frame.set(start + i);
        }
        return true;
    }

    public boolean checkCondition(final String property, final TelegramChunk chunk, final List<Integer> cases) {
        if (!checkInt(property, chunk)) {
            return false;
        }
        final Set<Integer> values = newHashSet();
        for (final int c : cases) {
            if (!values.add(c)) {
                throw new RuntimeException("Duplicate case in condition " + property + " :" + c);
            }
            if (c % (1 << chunk.size()) != c) {
                throw new RuntimeException("Invalid case value in condition " + property + " :" + c);
            }
        }

        return true;
    }

    public CodecChecker copy() {
        return new CodecChecker(frame.get(0, frame.size()));
    }

    public boolean isValid() {
        final int index = frame.nextClearBit(0);
        if (frame.nextSetBit(index) == -1) {
            return true;
        }
        throw new RuntimeException("Bit " + index + " not used");
    }
}
