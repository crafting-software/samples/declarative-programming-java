package sample.declarative.codec;

import java.util.BitSet;

public final class Telegram {
    private final BitSet content;

    public static Telegram telegram(final String content) {
        return new Telegram(fromString(content.replaceAll("\\s*\\|\\s*", "")));
    }

    public static Telegram empty() {
        return new Telegram(new BitSet());
    }

    private Telegram(final BitSet content) {
        this.content = content;
    }

    public boolean getBool(final TelegramChunk chunk) {
        return chunk.getBoolOf(content);
    }

    public Telegram setBool(final TelegramChunk chunk, final boolean value) {
        chunk.setBool(content, value);
        return this;
    }

    public Telegram setInt(final TelegramChunk chunk, final int value) {
        chunk.setInt(content, value);
        return this;
    }

    public int getInt(final TelegramChunk chunk) {
        return chunk.getIntOf(content);
    }

    public String content() {
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < content.size(); i++) {
            if (i % 8 == 0) {
                builder.append(" | ");
            }
            builder.append(content.get(i) ? "1" : "0");
        }
        builder.append(" | ");
        return builder.toString();
    }

    private static BitSet fromString(final String binary) {
        final BitSet bitset = new BitSet(binary.length());
        for (int i = 0; i < binary.length(); i++) {
            if (binary.charAt(i) == '1') {
                bitset.set(i);
            }
        }
        return bitset;
    }
}
