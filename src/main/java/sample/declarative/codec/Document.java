package sample.declarative.codec;

public interface Document {
    void beginSection(String name, String description);

    void endSection(String name, String description);

    void integerRule(TelegramChunk chunk, String property);

    void booleanRule(TelegramChunk chunk, String property);

    void startConditional(TelegramChunk chunk, String property);

    void endConditional(TelegramChunk chunk, String property);

    void startWhen(int value);

    void endWhen(int value);
}
