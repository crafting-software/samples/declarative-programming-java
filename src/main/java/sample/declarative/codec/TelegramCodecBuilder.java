package sample.declarative.codec;

public interface TelegramCodecBuilder {
    TelegramCodec decodeTo(String property);
}
