package sample.declarative.codec;

public final class Case {
    final int value;
    final TelegramCodec codec;

    public static Case on(final int value, final TelegramCodec codec) {
        return new Case(value, codec);
    }

    private Case(final int value, final TelegramCodec codec) {
        this.value = value;
        this.codec = codec;
    }
}
