package sample.declarative.codec;


public interface TelegramCodec {
    void decode(final Telegram telegram, final Signal signal);

    void encode(final Telegram telegram, final Signal signal);

    boolean check(final CodecChecker checker);

    String document();

    void document(final Document document);
}
