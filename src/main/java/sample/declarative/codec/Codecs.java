package sample.declarative.codec;

import java.util.Arrays;
import java.util.stream.Collectors;


public final class Codecs {


    public static TelegramCodec named(final String name, final String description, final TelegramCodec codec) {
        return new TelegramCodec() {
            public void decode(final Telegram telegram, final Signal signal) {
                codec.decode(telegram, signal);
            }

            public void encode(final Telegram telegram, final Signal signal) {
                codec.encode(telegram, signal);
            }

            public boolean check(final CodecChecker checker) {
                return codec.check(checker);
            }

            public String document() {
                return codec.document();
            }

            public void document(final Document document) {
                document.beginSection(name, description);
                codec.document(document);
                document.endSection(name, description);
            }
        };
    }


    public static TelegramCodec compose(final TelegramCodec... codecs) {
        return new TelegramCodec() {
            public void decode(final Telegram telegram, final Signal signal) {
                for (final TelegramCodec codec : codecs) {
                    codec.decode(telegram, signal);
                }
            }

            public void encode(final Telegram telegram, final Signal signal) {
                for (final TelegramCodec codec : codecs) {
                    codec.encode(telegram, signal);
                }
            }

            public boolean check(final CodecChecker checker) {
                return Arrays.stream(codecs).map((c) -> c.check(checker)).reduce((u, v) -> u && v).orElse(false);
            }

            public String document() {
                return Arrays.stream(codecs).map(TelegramCodec::document).collect(Collectors.joining("\n"));
            }

            public void document(final Document document) {
                for (final TelegramCodec codec : codecs) {
                    codec.document(document);
                }
            }
        };
    }

    public static TelegramCodec condition(final TelegramChunk chunk, final String property, final Case... cases) {
        return new TelegramCodec() {
            public void decode(final Telegram telegram, final Signal signal) {
                final int selector = telegram.getInt(chunk);
                for (final Case c : cases) {
                    if (c.value == selector) {
                        c.codec.decode(telegram, signal);
                    }
                }
                signal.setInt(property, selector);
            }

            public void encode(final Telegram telegram, final Signal signal) {
                final int selector = signal.getInt(property);
                for (final Case c : cases) {
                    if (c.value == selector) {
                        c.codec.encode(telegram, signal);
                    }
                }
                telegram.setInt(chunk, selector);
            }

            public boolean check(final CodecChecker checker) {
                return checker.checkCondition(property, chunk, Arrays.stream(cases).map(c1 -> c1.value).collect(Collectors.toList()))
                        && Arrays.stream(cases).map(c -> c.codec.check(checker.copy())).reduce((u, v) -> u && v).orElse(false);
            }

            public String document() {
                return "Switch " + chunk.document() + ":\n" +
                        Arrays.stream(cases).map(c -> "- " + c.value + ": " + c.codec.document()).collect(Collectors.joining("\n"));
            }

            public void document(final Document document) {
                document.startConditional(chunk, property);
                for (final Case c : cases) {
                    document.startWhen(c.value);
                    c.codec.document(document);
                    document.endWhen(c.value);
                }
                document.endConditional(chunk, property);
            }
        };
    }

    public static TelegramCodec bool(final TelegramChunk chunk, final String property) {
        return new TelegramCodec() {
            public void decode(final Telegram telegram, final Signal signal) {
                signal.setBool(property, telegram.getBool(chunk));
            }

            public void encode(final Telegram telegram, final Signal signal) {
                telegram.setBool(chunk, signal.getBool(property));

            }

            public boolean check(final CodecChecker checker) {
                return checker.checkBool(property, chunk);
            }

            public String document() {
                return "boolean at " + chunk.document() + " is property " + property;
            }

            public void document(final Document document) {
                document.booleanRule(chunk, property);
            }
        };
    }

    public static TelegramCodec integer(final TelegramChunk chunk, final String property) {
        return new TelegramCodec() {
            public void decode(final Telegram telegram, final Signal signal) {
                signal.setInt(property, telegram.getInt(chunk));
            }

            public void encode(final Telegram telegram, final Signal signal) {
                telegram.setInt(chunk, signal.getInt(property));
            }

            public boolean check(final CodecChecker checker) {
                return checker.checkInt(property, chunk);
            }

            public String document() {
                return "integer at " + chunk.document() + " is property " + property;
            }

            public void document(final Document document) {
                document.integerRule(chunk, property);
            }
        };
    }
}
